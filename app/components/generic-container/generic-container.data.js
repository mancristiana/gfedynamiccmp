"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var generic_data_1 = require("../generic.data");
/**
 * GenericContainerData must be extended by Data classes of Components such as Layout or Tabs that have the purpose of displaying other Components
 */
var GenericContainerData = (function (_super) {
    __extends(GenericContainerData, _super);
    function GenericContainerData() {
        _super.apply(this, arguments);
    }
    return GenericContainerData;
}(generic_data_1.GenericData));
exports.GenericContainerData = GenericContainerData;
//# sourceMappingURL=generic-container.data.js.map