import {Injectable, ComponentFactoryResolver, ComponentFactory, Compiler} from "@angular/core";
import {GenericService} from "./generic.service";
import {EComponent} from "./e.component";
import {ELabelComponent} from "./label/e-label.component";
import {ELabelData} from "./label/e-label.data";
import {ELayoutComponent} from "./layout/e-layout.component";
import {ELayoutData} from "./layout/e-layout.data";
import {ELabelView} from "./label/e-label.view";
import {ELayoutView} from "./layout/e-layout.view";

@Injectable()
export class GenericFactory {
    private controller: GenericService;
    constructor(private componentResolver: Compiler) {}

    setController(controller : GenericService) {
        this.controller = controller;
    }

    createInstance(type: string) : EComponent {
        switch (type) {
            case "label"            : return new ELabelComponent({controller: this.controller, data: new ELabelData(), factory: this.createLabelFactory() });
            case "layout"           : return new ELayoutComponent({controller: this.controller, data: new ELayoutData(), factory: this.createLayoutFactory() });
            default                 : return null;
        }
    }

    private createLabelFactory() : Promise<ComponentFactory<ELabelView>> {
        return this.componentResolver
           .compileComponentAsync(ELabelView)
            .then((factory: ComponentFactory<ELabelView>) => {
                return factory;
            });
    }

    private createLayoutFactory() : Promise<ComponentFactory<ELayoutView>> {
        return this.componentResolver
           .compileComponentAsync(ELayoutView)
            .then((factory: ComponentFactory<ELayoutView>) => {
                return factory;
            });
    }
}