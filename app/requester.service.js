"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require('rxjs/add/operator/toPromise');
require('rxjs/add/operator/map');
var RequesterService = (function () {
    function RequesterService(http) {
        this.http = http;
    }
    RequesterService.prototype.login = function () {
        var url = 'https://test.edillion.dk/faces/AppGateway?login';
        var body = 'email=cma@edillion.net&password=Gip9#pa5';
        // Content-Type: application/x-www-form-urlencoded
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(url, body, options)
            .toPromise()
            .then(function (res) { return res; });
    };
    RequesterService.prototype.placeRequest = function (method, callback, json) {
        var _this = this;
        if (!this.apiUrl)
            return this.get("config.json")
                .then(function (res) {
                _this.apiUrl = res.apiUrl;
                console.log(_this.apiUrl + callback);
                console.log(_this.apiUrl);
                return _this.switcher(method, callback, json);
            })
                .catch(function () {
                _this.apiUrl = window.location.protocol + "//" + window.location.host + "/a/api/";
                return _this.switcher(method, callback, json);
            });
        return this.switcher(method, callback, json);
    };
    RequesterService.prototype.switcher = function (method, callback, json) {
        var url = this.apiUrl + callback;
        switch (method) {
            case http_1.RequestMethod.Get:
                return this.get(url);
            // case 'POST' :
            //     return this.post(url, json);
            //     break;
            // case 'PUT' :
            //     return this.put(url, json);
            //     break;
            // case 'DELETE' :
            //     return this.delete(url);
            //     break;
            default: return;
        }
    };
    RequesterService.prototype.get = function (url) {
        return this.http.get(url)
            .toPromise()
            .then(function (res) {
            console.log(res);
            return JSON.parse(res.text());
        })
            .catch(this.handleError);
    };
    RequesterService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Promise.reject(errMsg);
    };
    RequesterService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], RequesterService);
    return RequesterService;
}());
exports.RequesterService = RequesterService;
//# sourceMappingURL=requester.service.js.map