"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var generic_data_1 = require("../generic.data");
var ELabelData = (function (_super) {
    __extends(ELabelData, _super);
    function ELabelData() {
        _super.apply(this, arguments);
    }
    ELabelData.prototype.configure = function (data) {
        this.text = data.text;
        this.type = data.type;
    };
    return ELabelData;
}(generic_data_1.GenericData));
exports.ELabelData = ELabelData;
//# sourceMappingURL=e-label.data.js.map