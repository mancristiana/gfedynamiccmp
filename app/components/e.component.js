"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var generic_component_1 = require("./generic.component");
/**
 * The purpose of this class is simplifying coding when doing polimorfic operations with GenericComponents
 */
var EComponent = (function (_super) {
    __extends(EComponent, _super);
    function EComponent() {
        _super.apply(this, arguments);
    }
    return EComponent;
}(generic_component_1.GenericComponent));
exports.EComponent = EComponent;
//# sourceMappingURL=e.component.js.map