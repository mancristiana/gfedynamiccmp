"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var generic_container_component_1 = require("./generic-container.component");
/**
 * The purpose of this class is simplifying coding when doing polimorfic operations with GenericContainerComponents
 */
var EContainerComponent = (function (_super) {
    __extends(EContainerComponent, _super);
    function EContainerComponent() {
        _super.apply(this, arguments);
    }
    return EContainerComponent;
}(generic_container_component_1.GenericContainerComponent));
exports.EContainerComponent = EContainerComponent;
//# sourceMappingURL=e-container.component.js.map