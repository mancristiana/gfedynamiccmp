import {Injectable} from '@angular/core';
import {RequestMethod} from "@angular/http";

import {AppComponent} from "../app.component";
import {GenericFactory} from "./generic.factory";
import {GenericData} from "./generic.data";

import {EComponent} from "./e.component";
import {EContainerComponent} from "./generic-container/e-container.component";

import {MockRequesterService} from "../mock-requester.service";
import {RequesterService} from "../requester.service";

@Injectable()
export class GenericService {

    app: AppComponent;

    componentsMap: { [key:string] : EComponent } = {};

    constructor(private requester: MockRequesterService,
                private factory: GenericFactory) {
        factory.setController(this);
    }

    setApp(app: AppComponent) {
        this.app = app;
    }

    /**
     * This method is called during the execution of a component's onNgInit method and it uses Requester for retrieving data and then runs the json processor
     * @param callback string in the following format "<GET | PUT | POST | DELETE> <API_REQUEST_URL>"
     * @param data optional json object required for PUT, POST, possibly DELETE
     */
    run( method: RequestMethod, callback : string, data? : any) : void {
        this.requester.placeRequest(method, callback, data)
            .then(data => this.processJson(data));
    }

    runClientAction(jsonData : any) {
        this.processJson(jsonData);
    }

    /**
     * This method configures a map with the components described in jsonData
     * @param jsonData json object containing an array of components
     */
    processJson(jsonData) : void {


        for(let component of jsonData.components) {

            // Attempt to find the component if already exists in the map
            var cmp = this.find(component.id);
            if(cmp === undefined) {
                // If not found then create a new instance of the component
                console.log("Creating " + component.type);
                cmp = this.create(component.type);

                // Add the instance to the map
                this.componentsMap[component.id] = cmp;
            }

            if (component.parentid === 'root') {
                console.log("Setting layout in master");
                this.app.setLayout(cmp);
            } else {
                var container = this.find(component.parentid);
                (<EContainerComponent>  container).placeChild(component.placement, cmp);
            }

            // Configure component's data
            this.configure(cmp, component);
        }
    }

    /**
     * This method looks into the local map (cache) for the component with specified id.
     * @param id component id
     * @return GenericData representing the component's data structure
     */
    find(id: string) : EComponent {
        return this.componentsMap[id];
    }

    /**
     *
     * @param type
     * @returns instance of GenericData
     */
    create(type : string) : EComponent {
        return this.factory.createInstance(type);
    }

    /**
     *
     * @param component
     * @param data
     */
    configure(component : EComponent, data : any) : void {
        component.configure(data);
    }

    /**
     * This method is used only for test and it is not necessary, so it can be deleted
     * @param data
     */
    display(data: any) {
        console.log(JSON.stringify(data));
    }

    processAction(json) {
        let cmp = JSON.parse(json).data;
        console.log(cmp);
        if (cmp.externalAction) {
            window.location.href = cmp.externalAction;
        }
        if (cmp.clientAction) {
            this.runClientAction(cmp.clientAction);
        }
        if (cmp.serverAction) {
            this.run(RequestMethod.Get, cmp.serverAction);
        }
    }
}