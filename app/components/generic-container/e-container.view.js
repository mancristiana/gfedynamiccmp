"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var EContainerView = (function () {
    function EContainerView() {
    }
    EContainerView.prototype.ngOnChanges = function () {
        var cmp = this.component;
        if (cmp) {
            cmp.createView(this.contentTarget);
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], EContainerView.prototype, "component", void 0);
    __decorate([
        core_1.ViewChild('content', { read: core_1.ViewContainerRef }), 
        __metadata('design:type', core_1.ViewContainerRef)
    ], EContainerView.prototype, "contentTarget", void 0);
    EContainerView = __decorate([
        core_1.Component({
            selector: 'e-container',
            template: "\n        <div #content></div>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], EContainerView);
    return EContainerView;
}());
exports.EContainerView = EContainerView;
//# sourceMappingURL=e-container.view.js.map