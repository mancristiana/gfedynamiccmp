"use strict";
var GenericComponent = (function () {
    function GenericComponent(component) {
        this.controller = component.controller;
        this.data = component.data;
        this.viewFactory = component.factory;
    }
    GenericComponent.prototype.createView = function (target) {
        var _this = this;
        return this.getFactory().then(function (factory) {
            target.clear();
            _this.activeView = target.createComponent(factory, 0).instance;
            _this.activeView.setData(_this.getData());
            _this.activeView.setController(_this.controller);
        });
    };
    GenericComponent.prototype.configure = function (jsonData) {
        this.data.configure(jsonData.data);
        this.data.setJson(jsonData);
    };
    GenericComponent.prototype.getFactory = function () {
        return this.viewFactory;
    };
    GenericComponent.prototype.getData = function () {
        return this.data;
    };
    return GenericComponent;
}());
exports.GenericComponent = GenericComponent;
//# sourceMappingURL=generic.component.js.map