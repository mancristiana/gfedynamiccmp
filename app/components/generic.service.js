"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require("@angular/http");
var generic_factory_1 = require("./generic.factory");
var mock_requester_service_1 = require("../mock-requester.service");
var GenericService = (function () {
    function GenericService(requester, factory) {
        this.requester = requester;
        this.factory = factory;
        this.componentsMap = {};
        factory.setController(this);
    }
    GenericService.prototype.setApp = function (app) {
        this.app = app;
    };
    /**
     * This method is called during the execution of a component's onNgInit method and it uses Requester for retrieving data and then runs the json processor
     * @param callback string in the following format "<GET | PUT | POST | DELETE> <API_REQUEST_URL>"
     * @param data optional json object required for PUT, POST, possibly DELETE
     */
    GenericService.prototype.run = function (method, callback, data) {
        var _this = this;
        this.requester.placeRequest(method, callback, data)
            .then(function (data) { return _this.processJson(data); });
    };
    GenericService.prototype.runClientAction = function (jsonData) {
        this.processJson(jsonData);
    };
    /**
     * This method configures a map with the components described in jsonData
     * @param jsonData json object containing an array of components
     */
    GenericService.prototype.processJson = function (jsonData) {
        for (var _i = 0, _a = jsonData.components; _i < _a.length; _i++) {
            var component = _a[_i];
            // Attempt to find the component if already exists in the map
            var cmp = this.find(component.id);
            if (cmp === undefined) {
                // If not found then create a new instance of the component
                console.log("Creating " + component.type);
                cmp = this.create(component.type);
                // Add the instance to the map
                this.componentsMap[component.id] = cmp;
            }
            if (component.parentid === 'root') {
                console.log("Setting layout in master");
                this.app.setLayout(cmp);
            }
            else {
                var container = this.find(component.parentid);
                container.placeChild(component.placement, cmp);
            }
            // Configure component's data
            this.configure(cmp, component);
        }
    };
    /**
     * This method looks into the local map (cache) for the component with specified id.
     * @param id component id
     * @return GenericData representing the component's data structure
     */
    GenericService.prototype.find = function (id) {
        return this.componentsMap[id];
    };
    /**
     *
     * @param type
     * @returns instance of GenericData
     */
    GenericService.prototype.create = function (type) {
        return this.factory.createInstance(type);
    };
    /**
     *
     * @param component
     * @param data
     */
    GenericService.prototype.configure = function (component, data) {
        component.configure(data);
    };
    /**
     * This method is used only for test and it is not necessary, so it can be deleted
     * @param data
     */
    GenericService.prototype.display = function (data) {
        console.log(JSON.stringify(data));
    };
    GenericService.prototype.processAction = function (json) {
        var cmp = JSON.parse(json).data;
        console.log(cmp);
        if (cmp.externalAction) {
            window.location.href = cmp.externalAction;
        }
        if (cmp.clientAction) {
            this.runClientAction(cmp.clientAction);
        }
        if (cmp.serverAction) {
            this.run(http_1.RequestMethod.Get, cmp.serverAction);
        }
    };
    GenericService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [mock_requester_service_1.MockRequesterService, generic_factory_1.GenericFactory])
    ], GenericService);
    return GenericService;
}());
exports.GenericService = GenericService;
//# sourceMappingURL=generic.service.js.map