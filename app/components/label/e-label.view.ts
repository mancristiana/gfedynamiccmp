import {Component} from "@angular/core";
import {GenericView} from "../generic.view";
import {ELabelData} from "./e-label.data";

@Component({
    selector: 'e-label',
    template: `
        <div class="e-text" (click)="onActionClicked()">
            <span [class.e-label-alert]="data.type === 'alert'">{{data.text}}</span>
        </div>
    `
})
export class ELabelView extends GenericView<ELabelData> {}