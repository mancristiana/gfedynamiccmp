export abstract class GenericData {
    // This field is used for demo purposes
    json : string;
    /**
     * The implementation of this method should initialize the component with data provided
     * by constructing it's corresponding specific data model
     * @param data Object containing data needed for configuring the component
     */
    abstract configure(data: any) : void;

    setJson(data : any) {
        this.json = JSON.stringify(data, null, 4);
    }

}