"use strict";
var GenericView = (function () {
    function GenericView() {
    }
    GenericView.prototype.setData = function (data) {
        this.data = data;
    };
    ;
    GenericView.prototype.setController = function (controller) {
        this.controller = controller;
    };
    GenericView.prototype.onActionClicked = function () {
        this.controller.processAction(this.data.json);
    };
    return GenericView;
}());
exports.GenericView = GenericView;
//# sourceMappingURL=generic.view.js.map