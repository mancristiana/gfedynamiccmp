"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var generic_component_1 = require("../generic.component");
/**
 * This class must be extended by Container Components such as Layout or Tabs that have the purpose of displaying other Components
 */
var GenericContainerComponent = (function (_super) {
    __extends(GenericContainerComponent, _super);
    function GenericContainerComponent() {
        _super.apply(this, arguments);
    }
    /**
     * This method manipulates GenericContainerData instance to include the EComponent in corresponding placement
     * @param placement string identifying the part of the container component where the EComponent should be displayed.
     *        The placement names vary and are specified for each type of container.
     * @param component EComponent that is to be included in the EContainerComponent
     */
    GenericContainerComponent.prototype.placeChild = function (placement, component) {
        this.data.setChild(placement, component);
    };
    return GenericContainerComponent;
}(generic_component_1.GenericComponent));
exports.GenericContainerComponent = GenericContainerComponent;
//# sourceMappingURL=generic-container.component.js.map