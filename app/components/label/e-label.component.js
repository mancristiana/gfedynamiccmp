"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var generic_component_1 = require("../generic.component");
var ELabelComponent = (function (_super) {
    __extends(ELabelComponent, _super);
    function ELabelComponent() {
        _super.apply(this, arguments);
    }
    return ELabelComponent;
}(generic_component_1.GenericComponent));
exports.ELabelComponent = ELabelComponent;
//# sourceMappingURL=e-label.component.js.map