import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions, RequestMethod} from "@angular/http";

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class RequesterService {
    // private domain: string = 'https://test.edillion.dk/api/';
    private apiUrl: string;// = 'https://192.168.43.146:8181/a/api/';

    constructor(private http : Http) {}

    login () {
        let url = 'https://test.edillion.dk/faces/AppGateway?login';
        let body = 'email=cma@edillion.net&password=Gip9#pa5';
        // Content-Type: application/x-www-form-urlencoded
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, body, options)
            .toPromise()
            .then(res => { return res});
    }

     placeRequest(
        method : RequestMethod,
        callback : string,
        json? : any
    ) {
        if(!this.apiUrl)
            return this.get("config.json")
                .then(res => {
                    this.apiUrl = res.apiUrl;
                    console.log(this.apiUrl + callback);
                    console.log(this.apiUrl);
                    return this.switcher(method, callback, json);
                })
                .catch(() => {
                    this.apiUrl = window.location.protocol + "//" + window.location.host + "/a/api/";
                    return this.switcher(method, callback, json);
                });
         return this.switcher(method, callback, json);
     }


    private switcher( method : RequestMethod,
                      callback : string,
                      json? : any)
    {

        var url = this.apiUrl + callback;
        switch(method) {
            case RequestMethod.Get :
                return this.get(url);
            // case 'POST' :
            //     return this.post(url, json);
            //     break;
            // case 'PUT' :
            //     return this.put(url, json);
            //     break;
            // case 'DELETE' :
            //     return this.delete(url);
            //     break;
            default : return;
        }
    }

    private get(url: string):  Promise<any> {
        return this.http.get(url)
            .toPromise()
            .then(res => {
                console.log(res);
                return JSON.parse(res.text()) })
            .catch(this.handleError);
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Promise.reject(errMsg);
    }
}