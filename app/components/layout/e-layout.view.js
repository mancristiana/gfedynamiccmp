"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var generic_view_1 = require("../generic.view");
var e_container_view_1 = require("../generic-container/e-container.view");
var ELayoutView = (function (_super) {
    __extends(ELayoutView, _super);
    function ELayoutView() {
        _super.apply(this, arguments);
    }
    ELayoutView = __decorate([
        core_1.Component({
            selector: 'e-layout',
            directives: [e_container_view_1.EContainerView],
            template: "\n    <div class=\"e-header-fixed\">\n        <e-container [component]=\"data.containers[6].component\"></e-container>\n    </div>\n    <div class=\"e-container\" \n        [class.e-has-header]=\"data.containers[6].component\"\n        [class.e-has-footer]=\"data.containers[7].component\">\n        \n        <div class=\"e-row\">\n            <!--<div [class.e-content]=\"data.containers[0].component\">-->\n                <e-container [component]=\"data.containers[0].component\"></e-container>\n            <!--</div>-->\n        </div>\n        <div class=\"e-row\">\n            <div class=\"e-col-l3\">\n                 <div [class.e-content]=\"data.containers[1].component\">\n                    <e-container [component]=\"data.containers[1].component\"></e-container>\n                </div>\n            </div>\n            <div [ngClass]=\"{\n                'e-col-l6'   : data.getCenterSize() === 2,\n                'e-col-l9'   : data.getCenterSize() === 1,\n                'e-col-l12'  : data.getCenterSize() === 0\n                }\">\n            \n                 <div [class.e-content]=\"data.containers[2].component\">\n                     <e-container [component]=\"data.containers[2].component\"></e-container>\n                     <e-container [component]=\"data.containers[5].component\"></e-container>\n                 </div>\n            </div>\n            <div class=\"e-col-l3\">\n                <div [class.e-content]=\"data.containers[3].component\">\n                    <e-container [component]=\"data.containers[3].component\"></e-container>\n                </div>\n            </div>\n        </div>\n        <div class=\"e-row\">\n            <div [class.e-content]=\"data.containers[4].component\">\n                <e-container [component]=\"data.containers[4].component\"></e-container>\n            </div>\n        </div>\n       \n        <div class=\"e-footer-fixed\" [hidden]=\"!data.containers[7].component\">\n            <e-container [component]=\"data.containers[7].component\"></e-container>\n        </div>\n    </div>"
        }), 
        __metadata('design:paramtypes', [])
    ], ELayoutView);
    return ELayoutView;
}(generic_view_1.GenericView));
exports.ELayoutView = ELayoutView;
//# sourceMappingURL=e-layout.view.js.map