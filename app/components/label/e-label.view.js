"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var generic_view_1 = require("../generic.view");
var ELabelView = (function (_super) {
    __extends(ELabelView, _super);
    function ELabelView() {
        _super.apply(this, arguments);
    }
    ELabelView = __decorate([
        core_1.Component({
            selector: 'e-label',
            template: "\n        <div class=\"e-text\" (click)=\"onActionClicked()\">\n            <span [class.e-label-alert]=\"data.type === 'alert'\">{{data.text}}</span>\n        </div>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], ELabelView);
    return ELabelView;
}(generic_view_1.GenericView));
exports.ELabelView = ELabelView;
//# sourceMappingURL=e-label.view.js.map