"use strict";
var GenericData = (function () {
    function GenericData() {
    }
    GenericData.prototype.setJson = function (data) {
        this.json = JSON.stringify(data, null, 4);
    };
    return GenericData;
}());
exports.GenericData = GenericData;
//# sourceMappingURL=generic.data.js.map