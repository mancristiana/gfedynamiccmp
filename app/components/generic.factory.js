"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var e_label_component_1 = require("./label/e-label.component");
var e_label_data_1 = require("./label/e-label.data");
var e_layout_component_1 = require("./layout/e-layout.component");
var e_layout_data_1 = require("./layout/e-layout.data");
var e_label_view_1 = require("./label/e-label.view");
var e_layout_view_1 = require("./layout/e-layout.view");
var GenericFactory = (function () {
    function GenericFactory(componentResolver) {
        this.componentResolver = componentResolver;
    }
    GenericFactory.prototype.setController = function (controller) {
        this.controller = controller;
    };
    GenericFactory.prototype.createInstance = function (type) {
        switch (type) {
            case "label": return new e_label_component_1.ELabelComponent({ controller: this.controller, data: new e_label_data_1.ELabelData(), factory: this.createLabelFactory() });
            case "layout": return new e_layout_component_1.ELayoutComponent({ controller: this.controller, data: new e_layout_data_1.ELayoutData(), factory: this.createLayoutFactory() });
            default: return null;
        }
    };
    GenericFactory.prototype.createLabelFactory = function () {
        return this.componentResolver
            .compileComponentAsync(e_label_view_1.ELabelView)
            .then(function (factory) {
            return factory;
        });
    };
    GenericFactory.prototype.createLayoutFactory = function () {
        return this.componentResolver
            .compileComponentAsync(e_layout_view_1.ELayoutView)
            .then(function (factory) {
            return factory;
        });
    };
    GenericFactory = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [core_1.Compiler])
    ], GenericFactory);
    return GenericFactory;
}());
exports.GenericFactory = GenericFactory;
//# sourceMappingURL=generic.factory.js.map