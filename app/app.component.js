"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require("@angular/http");
var e_layout_view_1 = require("./components/layout/e-layout.view");
var generic_service_1 = require("./components/generic.service");
var AppComponent = (function () {
    function AppComponent(genericController) {
        this.genericController = genericController;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.genericController.setApp(this);
        //this.genericController.run(RequestMethod.Get, 'vendors');
        // this.genericController.run(RequestMethod.Get, 'demo');
        this.genericController.run(http_1.RequestMethod.Get, 'expense/groups');
        // this.genericController.run(RequestMethod.Get, 'frontpage');
    };
    AppComponent.prototype.setLayout = function (component) {
        var cmp = component;
        if (cmp) {
            cmp.createView(this.layoutTarget);
        }
    };
    __decorate([
        core_1.ViewChild('layout', { read: core_1.ViewContainerRef }), 
        __metadata('design:type', core_1.ViewContainerRef)
    ], AppComponent.prototype, "layoutTarget", void 0);
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            directives: [e_layout_view_1.ELayoutView],
            template: '<div #layout></div>'
        }), 
        __metadata('design:paramtypes', [generic_service_1.GenericService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map