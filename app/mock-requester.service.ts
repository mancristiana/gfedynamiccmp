import {Injectable} from '@angular/core';
import {RequestMethod} from "@angular/http";

@Injectable()
export class MockRequesterService {
    placeRequest(method:RequestMethod, callback:string, json?:any):Promise<any> {
        return Promise.resolve(
            {
                components: [
                    {
                        id: "home",
                        parentid: "root",
                        placement: "west",
                        type: "layout",
                        data: [
                            "north",
                            "west",
                            "center",
                            "east",
                            "south"
                        ]
                    },
                    {
                        id: "label-id",
                        parentid: "home",
                        placement: "north",
                        type: "label",
                        data: {
                            text: "something"
                        }
                    }
                ]
            }
        );
    }
}

