"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var generic_container_data_1 = require("../generic-container/generic-container.data");
var ELayoutData = (function (_super) {
    __extends(ELayoutData, _super);
    function ELayoutData() {
        _super.apply(this, arguments);
        this.containers = new Array();
    }
    ELayoutData.prototype.configure = function (data) {
        this.containers = [];
        for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
            var placement = data_1[_i];
            var container = {
                placement: placement,
                component: null
            };
            this.containers.push(container);
        }
        while (this.containers.length < 8) {
            var container = {
                placement: this.containers.length + "",
                component: null
            };
            this.containers.push(container);
        }
    };
    ELayoutData.prototype.setChild = function (placement, component) {
        for (var _i = 0, _a = this.containers; _i < _a.length; _i++) {
            var container = _a[_i];
            if (container.placement === placement) {
                container.component = component;
            }
        }
    };
    ELayoutData.prototype.getCenterSize = function () {
        var count = 0;
        for (var _i = 0, _a = this.containers; _i < _a.length; _i++) {
            var container = _a[_i];
            if (container.placement === 'east' || container.placement === 'west') {
                if (container.component)
                    count++;
            }
        }
        return count;
    };
    return ELayoutData;
}(generic_container_data_1.GenericContainerData));
exports.ELayoutData = ELayoutData;
//# sourceMappingURL=e-layout.data.js.map