import {ELayoutView} from "./e-layout.view";
import {ELayoutData} from "./e-layout.data";
import {GenericContainerComponent} from "../generic-container/generic-container.component";

export class ELayoutComponent extends GenericContainerComponent<ELayoutData, ELayoutView> {}
