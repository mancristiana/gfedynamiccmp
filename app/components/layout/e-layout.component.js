"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var generic_container_component_1 = require("../generic-container/generic-container.component");
var ELayoutComponent = (function (_super) {
    __extends(ELayoutComponent, _super);
    function ELayoutComponent() {
        _super.apply(this, arguments);
    }
    return ELayoutComponent;
}(generic_container_component_1.GenericContainerComponent));
exports.ELayoutComponent = ELayoutComponent;
//# sourceMappingURL=e-layout.component.js.map